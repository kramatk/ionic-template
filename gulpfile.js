var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var uglify = require('gulp-uglify');
var gutil = require('gulp-util');

gulp.task('sass', function() {
    return gulp.src([
        'www/asset/scss/*.scss'
    ])
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(concat('app.css'))
    .pipe(minifyCss())
    .pipe(rename('app.min.css'))
    .pipe(gulp.dest('www/css/'));
});

// Concatenate & Minify JS
gulp.task('js', function() {
    return gulp.src([
            'www/asset/js/app.js',
            'www/asset/js/**/*.js'
        ])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('www/js/'))
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(rename('app.min.js'))
        .pipe(gulp.dest('www/js/'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('www/asset/js/**/*.js', ['js']);
    gulp.watch('www/asset/scss/*.scss', ['sass']);
});

// Default Task
gulp.task('default', ['sass','js', 'watch']);

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
